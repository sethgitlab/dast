# ZAP Server Command Line Interface

## About

The ZAP server accepts many command line interface arguments. These often allow low-level control of parameters that affect
how ZAP executes a scan. A non-exhaustive list of parameters has been [documented on an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/36437#note_244981023).

As it stands, the DAST team does not fully understand what each of these parameters does. It is likely that many are
not relevant to the context in which DAST runs a scan, however, there is a belief that understanding the parameters
will lead to more options for DAST users and therefore a better user experience.

This document outlines parameters that the DAST team have come to understand. As this list grows, it is anticipated that
it will be converted into DAST user documentation.

## Options

Please keep this list alphabetically ordered.

| Option                          | Default    | Description                                                                                             | Reference |
| ------------------------------- | ---------- | --------------------------------------------------------------------------------------------------------| --------- |
| `connection.timeoutInSecs`      | 20 seconds | The Connection and Socket Timeout for all requests made by ZAP                                        | [ConnectionParam.java](https://github.com/zaproxy/zaproxy/blob/d093982b2e79c23608e2edcd6fc29d1d833ac892/zap/src/main/java/org/parosproxy/paros/network/ConnectionParam.java#L185)
