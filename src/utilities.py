def is_url(string: str) -> bool:
    return string.startswith('http://') or string.startswith('https://')
