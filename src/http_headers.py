from operator import attrgetter


class HttpHeaders:

    def __init__(self, headers=[]):
        self.headers = headers

    def __len__(self):
        return len(self.headers)

    def to_list_of_dicts(self):
        return [header.to_dict() for header in self.sort_by_name().headers]

    def sort_by_name(self):
        headers = sorted(self.headers, key=attrgetter('name'))
        return HttpHeaders(headers)

    def mask(self, mask_headers=[]):
        case_insensitive_mask_headers = [name.lower() for name in mask_headers]
        headers = []

        for header in self.headers:
            if header.name.lower() in case_insensitive_mask_headers:
                headers.append(header.mask())
            else:
                headers.append(header)

        return HttpHeaders(headers)
