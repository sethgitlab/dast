#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/../.."

# Lint Shell scripts
# Exclude https://github.com/koalaman/shellcheck/wiki/SC1117 as it has been considered 'pedantic' in new versions of shellcheck
shellcheck --exclude=SC1117 ./analyze
find "$PROJECT_DIR" -name "*.sh" -print0 | xargs -0 shellcheck --exclude=SC1117
