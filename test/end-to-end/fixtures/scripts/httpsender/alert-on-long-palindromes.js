
// Vulnerability configuration
var pluginId = 9001
var naughtyPalindrome = 'Are we not drawn onward, we few, drawn onward to new era?'
var lowRisk = 1
var highConfidence = 3
var references = ['https://en.wiktionary.org/wiki/Appendix:English_palindromes']


// Import Java types
var Control = Java.type('org.parosproxy.paros.control.Control')
var Alert = Java.type('org.parosproxy.paros.core.scanner.Alert')
var HistoryReference = Java.type('org.parosproxy.paros.model.HistoryReference')
var Model = Java.type('org.parosproxy.paros.model.Model')


/**
 * Called before sending the request to the server.
 * See https://github.com/zaproxy/zaproxy/blob/7515fd13ab6efc8f8127d4e917728479dca3c63b/zap/src/main/java/org/zaproxy/zap/extension/script/ExtensionScript.java#L1764
 *
 * @param msg       the HTTP message being sent/received
 * @param initiator the initiator of the request, for possible values see see https://github.com/zaproxy/zaproxy/blob/161f3e9563d3eb5843f83aea91e59fc6ccb3e282/zap/src/main/java/org/parosproxy/paros/network/HttpSender.java#L129
 * @param helper    the helper class that allows to send other HTTP messages
 */
function sendingRequest(msg, initiator, helper) {
	// intentionally blank
}

/**
 * Called after receiving a response from the server.
 * See https://github.com/zaproxy/zaproxy/blob/7515fd13ab6efc8f8127d4e917728479dca3c63b/zap/src/main/java/org/zaproxy/zap/extension/script/ExtensionScript.java#L1764
 *
 * @param msg       the HTTP message being sent/received
 * @param initiator the initiator of the request, for possible values see see https://github.com/zaproxy/zaproxy/blob/161f3e9563d3eb5843f83aea91e59fc6ccb3e282/zap/src/main/java/org/parosproxy/paros/network/HttpSender.java#L129
 * @param helper    the helper class that allows to send other HTTP messages
 */
function responseReceived(msg, initiator, helper) {
    var alertExtensionName = org.zaproxy.zap.extension.alert.ExtensionAlert.NAME
    var alertManager = Control.getSingleton().getExtensionLoader().getExtension(alertExtensionName)

    if (alertManager != null && msg.getResponseBody().toString().contains(naughtyPalindrome)) {
        var alert = new Alert(pluginId, lowRisk, highConfidence, 'Long palindrome was detected')

        // retrieve the HttpMessage from the database
        var ref = msg.getHistoryRef()

        if (ref != null && HistoryReference.getTemporaryTypes().contains( java.lang.Integer.valueOf(ref.getHistoryType()))) {
            // Don't use temporary types as they will get deleted
            ref = null
        }
        if (ref == null) {
            // map the initiator
            var type
            switch (initiator) {
                case 1:	// PROXY_INITIATOR
                    type = 1 // Proxied
                    break
                case 2:	// ACTIVE_SCANNER_INITIATOR
                    type = 3 // Scanner
                    break
                case 3:	// SPIDER_INITIATOR
                    type = 2 // Spider
                    break
                case 4:	// FUZZER_INITIATOR
                    type = 8 // Fuzzer
                    break
                case 5:	// AUTHENTICATION_INITIATOR
                    type = 15 // User
                    break
                case 6:	// MANUAL_REQUEST_INITIATOR
                    type = 15 // User
                    break
                case 8:	// BEAN_SHELL_INITIATOR
                    type = 15 // User
                    break
                case 9:	// ACCESS_CONTROL_SCANNER_INITIATOR
                    type = 13 // Access control
                    break
                default:
                    type = 15 // User - fallback
                    break
            }
            ref = new HistoryReference(Model.getSingleton().getSession(), type, msg)
        }

        alert.setMessage(msg)
        alert.setUri(msg.getRequestHeader().getURI().toString())
        alert.setDescription('A long palindrome was found in the response body. This could be hiding a secret for those who read content right to left ;-).')
        alert.setEvidence(naughtyPalindrome)
        alert.setReference(references)
        alertManager.alertFound(alert, ref)
    }
}
