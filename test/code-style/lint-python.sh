#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/../.."

# Lint Python
find "$PROJECT_DIR" -name "*.py" -print0 | xargs -0 flake8 --show-source --config="$PROJECT_DIR/.flake8"
