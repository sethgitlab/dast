from __future__ import annotations

from enum import Enum


# There are many types of ZAP scripts, each of which are injected into the ZAP life cycle differently.
# Scripts here are those currently supported and known to work with DAST.
# See ZAP documentation for full list of scripts https://www.zaproxy.org/docs/desktop/addons/script-console/
class ScriptType(Enum):
    HTTP_SENDER = (0, 'httpsender')    # run against every request/response sent/received by ZAP

    def zap_name(self) -> str:
        return self.value[1]
